package com.example.rockstarchef;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import com.example.rockstarchef.ui.main.Login;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, Login.newInstance())
                    .commitNow();
        }
    }
}