package com.example.rockstarchef.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.textclassifier.TextClassification;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rockstarchef.MainActivity;
import com.example.rockstarchef.R;

import org.w3c.dom.Text;


public class Login extends Fragment {
    private MainViewModel mViewModel;

    public static Login newInstance() {
        return new Login();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View i = inflater.inflate(R.layout.fragment_login, container, false);
        final View tt = i.findViewById(R.id.textView5);

//        tt.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View view) {
//                ((TextView)tt).setText("Vikas");
//            }
//        });
        tt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View View) {
                Log.i("debug", "Button Clicked");

            }
        });
        return i;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }
}